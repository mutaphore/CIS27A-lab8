import java.util.Scanner;

/*
@author: Dewei Chen 
@date: 3-6-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a8_1.java
@description: This program reads in ten numbers and displays 
distinct numbers (i.e., if a number appears multiple times, 
it is displayed only once). 
*/

public class a8_1 {
	
	public static void main(String[] args) {
		
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		
		storeArray(array1,array2);
		displayArray(array2);
		
	}
	
	
	
	
	
	
	
	
	
	
	//Prompts user for 10 numbers and store them into array 1, also calls checkArray method
	public static void storeArray(int[] array1, int[] array2) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter ten numbers: ");
		
		for (int i=0 ; i<10 ; i++)
			array1[i] = input.nextInt();
		
		checkArray(array1,array2);
		
	}
	
	
	
	
	
	
	
	
	
	
	//Checks the array 1 for unique numbers and stores them into array 2
	public static void checkArray(int[] array1, int[] array2) {
		
		for (int i=0 ; i<10 ; i++) {
			
			for (int j=0 ; j<10 ; j++) {
				
				if (array1[i] == array2[j])
					j = 10;
				else if (array2[j] == 0) {
					array2[j] = array1[i];
					j = 10;
				}
				
			}
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	//Prints out an array (omitting any empty array spots)
	public static void displayArray(int[] array) {
		
		System.out.print("The distinct numbers are: ");
		
		for (int i=0 ; i<10 ; i++)
			if (array[i] != 0)
				System.out.print(array[i] + " ");

	}

}
